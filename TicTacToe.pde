int player1 = 1;                                          //состояние клеток, в которых стоит крестик
int player2 = -1;                                         //состояние клеток, в которых стоит нолик
int empty = 0;                                            //состояние пустых клеток

int gridSize = 4;                                         //размер поля (количество клеток по X и по Y)
int cellSize;                                             //размер одной клетки в пикселях
int[][] grid = new int[gridSize][gridSize];               //массив состояний клеток

PImage newgame;                                           //картинка "новая игра"

int score1;                                               //очки первого игрока
int score2;                                               //очки второго игрока

int whichTurn;                                            //чей ход
int numberMoves;                                          //количество сделаных ходов за текущую партию

boolean gameIsActive;

void setup() {
  newgame = loadImage("New.png");                        //подгрузка картинки
  size(400, 600);                                         //установка окна с раширением 400*600 пикселей
  strokeWeight(10);                                       //Толщина линий 10 пикселей
  cellSize = width/gridSize;                              //расчёт размера клетки
  textAlign(CENTER);                                      //центровка текста
  textSize(cellSize/2);                                   //размер текста - пол высоты клетки
  NewGame();                                              
  gameIsActive = true;
}

void draw() {
  if (mousePressed) {                                     //если нажата любая внопка мыши
    if (gameIsActive) {
      if (mouseY > cellSize && mouseY < 5 * cellSize) {   //если мышка находилась в этот момент над игровым полем
        int x = mouseX / cellSize;                        //перевод координаты х мышки (пиксели) в номер клетки по х (число)
        int y = mouseY / cellSize;                        //-||- но без учёта того, что сетка смещена
        if (grid[x][y - 1] == empty) {                    //если клетка была пустой (учитывается смещение сетки)   
          grid[x][y - 1] = whichTurn;                     //она стала с тем состоянием, которое соответствует текущему игроку
          DrawFigure(x, y, whichTurn);
          numberMoves++;
          whichTurn *= -1;
        }
      }

      if (CheckEnd())
        gameIsActive = false;
    }

    if (mouseX > width/2 - 66 && mouseX < width/2 + 66 && mouseY > cellSize/2 - 30 && mouseY < cellSize/2 + 30)
      NewGame();                                          //Нажатие на кнопку "новая игра". Её длина по Х - 132 пикселя, по Y - 60
  }
}


void DrawGrid() {                                         //Создание игровой сетки
  for (int i = 0; i <= gridSize; i++) {
    line(0, (i + 1) * cellSize, width, (i + 1) * cellSize);   
    line(i * cellSize, cellSize, i * cellSize, width + cellSize);
  }
}

void NewGame() {
  for (int i = 0; i < gridSize; i++)
    for (int j = 0; j < gridSize; j++)
      grid[i][j] = empty;                                 //все клетки теперь имеют состояние "пусто"
  numberMoves = 0;                                        //количество ходов за текущию партию сделать 0
  whichTurn = player1;                                    //первыми ходят крестики
  gameIsActive = true;

  background(200, 200, 200);                              //очистить окно от всего
  DrawGrid();                                             //нарисовать сетку
  image(newgame, width/2 - 66, cellSize/2 - 30);          //нарисовать кнопку "новая игра"

  line(cellSize * 2 / 5, 5 * cellSize + cellSize * 2 / 5, cellSize * 3 / 5, 5 * cellSize + cellSize * 3 / 5); //нарисовать маленький крестик для очков
  line(cellSize * 2 / 5, 5 * cellSize + cellSize * 3 / 5, cellSize * 3 / 5, 5 * cellSize + cellSize * 2 / 5);
  fill(200, 0, 0); 
  text(score1, cellSize, 5 * cellSize + cellSize * 5 / 7);//очки крестиков, красным цветом

  noFill();                                               //убрать заливку
  ellipse(2 * cellSize + cellSize * 1 / 2, 5 * cellSize + cellSize * 1 / 2, cellSize * 2 / 5, cellSize * 2 / 5); // нарисовать маленький нолик для очков
  fill(0, 0, 200);
  text(score2, 3 * cellSize, 5 * cellSize + cellSize * 5 / 7);//очки ноликов, синим цветом
}

void DrawFigure(int x, int y, int which) {                //Рисование фигуры при ходе. Игрок1 - крестик. Игрок2 - нолик
  noFill();                                               //что бы нолики не заливались
  x *= cellSize;                                          //x - номер текущей клетки по х. Это строчкой номер переводится в левую границу этой ячейки в пикселях
  y *= cellSize;                                          //-||-
  if (which == player1) {                                 //рисовка крестика, пропорционального размеру ячейки
    line(x + cellSize * 1 / 4, y + cellSize * 1 / 4, x + cellSize * 3 / 4, y + cellSize * 3 / 4);
    line(x + cellSize * 1 / 4, y + cellSize * 3 / 4, x + cellSize * 3 / 4, y + cellSize * 1 / 4);
  } else if (which == player2)                              //рисовка нолика, пропорционального размеру ячейки
    ellipse(x + cellSize * 1 / 2, y + cellSize * 1 / 2, cellSize / 2, cellSize / 2);
}

boolean CheckEnd() {
  if (Match4()) {
    score1++;
    fill(200, 0, 0); 
    text("+1", cellSize + cellSize / 2, 5 * cellSize + cellSize * 5 / 7);    //крестикам +очко
    return true;
  } else if (numberMoves == 16) {
    score2++;
    fill(0, 0, 200);
    text("+1", 3 * cellSize + cellSize / 2, 5 * cellSize + cellSize * 5 / 7);// ноликам +очко
    return true;
  }
  return false;
}

boolean Match4() {                                        //проверка конца игры условием 4 в ряд
  int sum;                                                //Сумма состояний клеток

  for (int i = 0; i < gridSize; i++) {                    //проверка по горизонталям
    sum = 0;                                              //сумма = 0 при начале подсчёта конкретной горизонтали
    for (int j = 0; j < gridSize; j++) {
      sum += grid[i][j];                                  //прибавляем состояние клетки
    }
    if (sum == gridSize * player1 || sum == gridSize * player2)
      return true;                                        // -> конец партии
  }

  for (int j = 0; j < gridSize; j++) {                    //по вертикалям, -||-
    sum = 0;
    for (int i = 0; i < gridSize; i++) {
      sum += grid[i][j];
    }
    if (sum == gridSize * player1 || sum == gridSize * player2)
      return true;
  }

  sum = 0;                                                //проверка главной диагонали
  for (int i = 0; i < gridSize; i++)
    sum+=grid[i][i];  
  if (sum == gridSize * player1 || sum == gridSize * player2)
    return true;

  return false;                                           //если нигде нет 4
}